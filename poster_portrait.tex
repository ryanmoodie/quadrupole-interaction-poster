\documentclass[portrait,a0paper,fontscale=0.292]{baposter}

\usepackage[vlined]{algorithm2e}
\usepackage{times}
\usepackage{calc}
\usepackage{url}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{relsize}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{multicol}
\usepackage[T1]{fontenc}
\usepackage{ae}
\usepackage{subfig}
\usepackage{ragged2e}
\usepackage{enumitem}
\usepackage{array}
\usepackage{adjustbox}
\usepackage{tabularx}
\usepackage{}

\graphicspath{{my_images/}}

\setlength{\columnsep}{0.7em}
\setlength{\columnseprule}{0mm}

\setlist[itemize]{leftmargin=2.2ex}

\setlength{\tabcolsep}{1mm}

\font\dsfnt=dsrom12
\color{black}
\hyphenpenalty=5000

\begin{document}

    \begin{poster}{
        colspacing=0.7em,
        headerColorOne=cyan!20!white!90!black,
        borderColor=cyan!30!white!90!black,
        textborder=faded,
        headerborder=open,
        headershape=roundedright,
        headershade=plain,
        boxshade=none,
        background=plain,
        bgColorOne=white,
        % bgColorTwo=cyan,
        headerheight=0.12\textheight
        }
        {
           \includegraphics[height=0.12\textheight]{university_of_st_andrews_logo}
            }
        {\sc\Huge Modelling Electron States in\\ \vspace{0.5ex}
        Silicon-Based Quantum Computers}
        {
            \vspace{1ex} Ryan Moodie$^{\textbf{1}}$, Giuseppe Pica$^{\textbf{1}}$ and Brendon Lovett$^{\textbf{1}}$\\\vspace{1ex}
            \large ${}^{\textbf{1}}$School of Physics and Astronomy, University of St Andrews, KY16 9SS, UK\\
            {\normalsize rim2@st-andrews.ac.uk}
            }
        {
           \begin{tabular}{r}
           \includegraphics[height=0.03\textheight]{epsrc_logo_vector_crop}
           \end{tabular}
            }

        %%% Now define the boxes that make up the poster
        %%%---------------------------------------------------------------------------
        %%% Each box has a name and can be placed absolutely or relatively.
        %%% The only inconvenience is that you can only specify a relative position 
        %%% towards an already declared box. So if you have a box attached to the 
        %%% bottom, one to the top and a third one which should be inbetween, you 
        %%% have to specify the top and bottom boxes before you specify the middle 
        %%% box.

        \headerbox{Abstract}{name=abstract,column=0,row=0,span=2}{

            We present a simulation of two electrons in silicon under the action of an externally introduced potential and analyse the quadrupole interaction between them. The simulation, written in Python, can model in two dimensions the planar silicon devices under current experimental research, calculating the electron wavefunctions using a self-consistent method based on a Schr\"{o}dinger-Poisson solver and determining interaction between them. Results correlate with ongoing research into exploitation of quadrupole coupling between qubits for manipulation of quantum information \cite{quadrupole}.
            }

        \headerbox{Planar Silicon Devices}{name=experimental,column=0,span=1,below=abstract}{

            \justify

                \begin{itemize}

                    \item In experimental research, planar MOS devices are used to create multiple-qubit systems required for full-scale quantum computers.

                    \item Design: 

                    \begin{itemize}

                        \item metal gate structure 

                        \item over insulating oxide layer

                        \item over silicon semiconductor substrate.

                        \end{itemize}

                    \item Voltages are applied to gates to control the potential in the silicon and create quantum dots to confine single electrons, the spin states of which encode quantum information to form qubits.

                    \end{itemize}

            % \vspace{3ex}
     
            }

        \headerbox{Potential Profile}{name=potential,column=0,span=1,below=experimental}{

            \justify

                \begin{itemize}

                    \item Sample gate structure:
                    
                    \begin{itemize}
                        \item two square gates of side length 2nm
                        \item set 100nm apart
                        \item respective applied voltages of -1V and -2V.
                        \end{itemize}

                    \item Potential profile at depth of 50nm from gates:

                    \end{itemize}

            \centering

                \includegraphics[width=\linewidth]{potential}

            \justify

                \begin{itemize}

                    \item Two confinement regions produced

                    \begin{itemize}
                        \item square solving region centred on each
                        \item tight-binding lattices shown.
                        \end{itemize}

                    \item Initial potential efficiently found using analytical solution of Poisson's equation:

                    \centering

                    \small

                    \[ \hspace{-3.5ex} V(r) = \int \frac{V_g(r)}{\pi} \arctan \left( \frac{z \sin(\theta)}{(r - x) \cos(\theta) - y \sin(\theta)} \right) d\textbf{\emph{a}} \]

                    \end{itemize}

            }

        \headerbox{Wavefunctions}{name=wavefunctions,column=1,below=abstract,span=1}{

            \centering

                \begin{tabularx}{\textwidth}{c c c}

                    \hspace{-1ex} Iteration \hspace{-3ex} & \hspace{1.3ex} Dot 0 & \hspace{1ex} Dot 1 \\

                    \begin{minipage}[b]{1em} 0 \vspace{9.5ex} \end{minipage} & \includegraphics[width=0.4\linewidth]{iteration_0_probability_density_0} &   \includegraphics[width=0.4\linewidth]{iteration_0_probability_density_1} \vspace{-1ex} \\

                     & \footnotesize \hspace{1em} (a) & \footnotesize \hspace{1em} (b) \\

                    \begin{minipage}[b]{1em} 1 \vspace{9.5ex} \end{minipage} & \includegraphics[width=0.4\linewidth]{iteration_1_probability_density_0} &   \includegraphics[width=0.4\linewidth]{iteration_1_probability_density_1} \vspace{-1ex} \\

                     & \footnotesize \hspace{1em} (c) & \footnotesize \hspace{1em} (d) \\

                    \begin{minipage}[b]{1em} 2 \vspace{9.5ex} \end{minipage} & \includegraphics[width=0.4\linewidth]{iteration_2_probability_density_0} &   \includegraphics[width=0.4\linewidth]{iteration_2_probability_density_1} \vspace{-1ex} \\

                     & \footnotesize \hspace{1em} (e) & \footnotesize \hspace{1em} (f) \vspace{-2ex} \\

                \end{tabularx}

            \justify

                \begin{itemize}

                    \item Tight-binding lattice set up in region 0 and Schr\"{o}dinger's Equation solved for first electron wavefunction (a).

                    \item Repeat for region 1 to determine second electron wavefunction (b) including Coulomb repulsion exerted on this electron from the first.

                    \item Return to first electron and solve for its wavefunction (c) under the electrostatic interaction of the second.

                    \item Repeat for second electron (d) with the new electrostatic interaction of the first electron.

                    \item This forms the second step in an iterative calculation, which continues until the eigenvalues of the systems converge, giving self-consistent wavefunctions of the two electrons, (e) and (f). 

                    \end{itemize}

            }

        \headerbox{Qubit Interaction}{name=interaction,column=1,below=wavefunctions,span=1,above=bottom}{

            \justify

                \begin{itemize}

                    \item Exchange interaction, used in the design of quantum logic gates, is a quantum effect arising from the requirement of a two electron wavefunction to be antisymmetric under particle exchange. 

                    \item Exchange coupling therefore depends on the electron wavefunctions' overlap, so is sensitive to tiny potential changes as the wavefunctions exponentially decay in the overlap region.

                    \item The quadrupole interaction, however, does not depend on wavefunction overlap and thus may be a more robust interaction to exploit.

                    \end{itemize}

            }

        \headerbox{Quadrupole Coupling}{name=quadrupole,column=2,span=1,row=0}{

            \justify

                \begin{itemize}

                    \item Quadrupole coupling is given by:

                    \[ E_Q = \frac{1}{6} \sum_{\alpha,\beta} Q_{\alpha\beta} V_{\alpha\beta}  \]

                    with externally applied electric field gradient \emph{V}\cite{slichter}. 

                    \item The quadrupole moment \emph{Q} has zero off-diagonal terms and diagonal terms given by: 

                    \[ Q_{\alpha\alpha} = \int |\psi|^2 (3\alpha^2-r^2) d\textbf{\emph{a}} \]

                    \end{itemize}
            }

        \headerbox{Results}{name=results,column=2,span=1,below=quadrupole}{
                
            \justify

                \begin{itemize}
                    \item Energies against simulation resolution:
                    \end{itemize}
             
            \centering

                \includegraphics[width=0.8\linewidth]{energies_against_resolution} 

            \justify

                \begin{itemize}


                    \item Quadrupole coupling against dot aspect ratio:

                    \end{itemize}

            \centering

                \includegraphics[width=0.8\linewidth]{quadrupole_coupling_against_aspect_ratio}  

            \justify

                \begin{itemize}

                    \item Quadrupole coupling against distance between dots:

                    \end{itemize}
              
            \centering

                \includegraphics[width=0.8\linewidth]{quadrupole_coupling_against_distance} 
           
            }

        \headerbox{Conclusion}{name=conclusion,column=2,span=1,below=results, above=bottom}{

            \justify

                \begin{itemize}

                    \item Prolate wavefunctions had greater quadrupole coupling.

                    \item Sufficient interaction at realistic scales for use in implementation of quantum logic gates.

                    \item Future work:

                    \begin{itemize}

                        \item Perhaps more accurate results at higher resolution.

                        \item Extend simulation to three dimensions.

                        \end{itemize}

                    \end{itemize}

            }

        \headerbox{References}{name=references,column=0,below=potential,above=bottom}{
         
            \smaller
             
                \bibliographystyle{ieee} 
                \renewcommand{\section}[2]{\vskip 0.05em}

               \begin{thebibliography}{1}\itemsep=-0.01em
                   \setlength{\baselineskip}{0.4em}

                    \bibitem{quadrupole}
                      P. A. Mortemousque et al.,
                      \emph{Quadrupole Shift of Nuclear Magnetic Resonance of Donors in Silicon at Low Magnetic Field},
                      \tt{arXiv:1506.04028v1 [cond-mat.mes-hall]},
                      \rm{Jul 2015.}

                    \bibitem{slichter}
                      C. P. Slichter,
                      ``Electrical Quadrupole Effects'' in
                      \emph{Principles of Magnetic Resonance},
                      vol. 1,
                      \emph{Springer Series in Solid-State Sciences},
                      3rd Ed.
                      Berlin, Germany:
                      Springer,
                      1989,
                      ch. 10,
                      sec. 2,
                      pp. 486-489.

                    \end{thebibliography}
            }

    \end{poster}

\end{document}
